const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('.models/Room')
mongoose.connect('mongoose://localhost:27017/example')
async function main () {
  const newInformaticsBuilding = await Building.findById('6216116555816c4f9cec8033')
  const room = await Room.findById('6216116555816c4f9cec8033')
  const informaticsBuilding = await Building.findById(room.Building)
  console.log(room)
  console.log(newInformaticsBuilding)
  console.log(informaticsBuilding)
  room.building = newInformaticsBuilding
  newInformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newInformaticsBuilding.save()
  informaticsBuilding.save()
}

main().then(function () {
  console.log('Finish')
})
