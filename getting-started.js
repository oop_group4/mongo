const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')

const kittySchema = new mongoose.Schema({
  name: String
})
kittySchema.method.speak = function speak () {
  const greeting = this.name
    ? 'Meow name is ' + this.name
    : "I don't have a name"
  console.log(greeting)
}
const Kitten = mongoose.model('Kitten', kittySchema)

const silence = new Kitten({ name: 'Silence' })
console.log(silence.name)
console.log(silence)
silence.speak()
// Promise
silence.save().then(function (resulf) {
  console.log(resulf)
}).catch(function (err) {
  console.log(err)
})
const fluffy = new Kitten({ name: 'fluffy' })
fluffy.speak()
fluffy.save(function (err, resulf) {
  if (err) {
    console.log(err)
  // eslint-disable-next-line no-empty
  } else {
    console.log(resulf)
  }
})

async function saveCat (name) {
  const cat = new Kitten({ name: name })
  try {
    const resulf = await cat.save()
    console.log(resulf)
    return resulf
  } catch (e) {
    console.log(e)
  }
}

saveCat('Ta').then((resulf) => {
  console.log(resulf)
})
