const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  name: { typr: String },
  floor: Number,
  room: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }]
})

module.exports = mongoose.model('Building', buildingSchema)
