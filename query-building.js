const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('.models/Room')
mongoose.connect('mongoose://localhost:27017/example')
async function main () {
  // Update
  // const room = await Room.findById('6216116555816c4f9cec8033')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  const room = await Room.findOne({ capacity: { $lt: 100 } }).populate('building')
  console.log(room)
  console.log('-------------------------')
  const rooms = await Room.find({ capacity: { $lt: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('room')
  console.log(JSON.stringify(building))
}

main().then(function () {
  console.log('Finish')
})
